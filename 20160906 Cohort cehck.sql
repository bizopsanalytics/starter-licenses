select  a.hosted_url, 
        a.tech_email_domain,
        a.sen,
        a.parent_sen,
        a.first_paid_user_limit, 
        a.last_paid_user_limit,
        a.paid_license_end_date,
        a.tech_country,
        b.company_size
from public.license as a
left join zone_bizops.customer_size as b on a.tech_email_domain = b.email_domain
where           a.base_product = 'JIRA Software'
and             a.hosted_url in (
                'geodetic.atlassian.net',
                'eurozet.atlassian.net',
                'sparkconnect.atlassian.net',
                'medycynapraktyczna.atlassian.net',
                'nucleusfi.atlassian.net',
                'codearchitects.atlassian.net',
                'enmacc.atlassian.net',
                'somproduct.atlassian.net',
                'filmwebfw.atlassian.net',
                'ccc-it.atlassian.net',
                'smartdatasoft.atlassian.net',
                'streamnow.atlassian.net',
                'lovehoney.atlassian.net',
                'bwhihub.atlassian.net',
                'anetcorp.atlassian.net',
                'labsquad.atlassian.net',
                'magplus.atlassian.net',
                'bymacht.atlassian.net',
                'sigmaestimates.atlassian.net',
                'indusa.atlassian.net',
                'mtlsoftware.atlassian.net',
                'infeedo.atlassian.net',
                'rightsource.atlassian.net',
                'ridemcts.atlassian.net',
                'tlprojects.atlassian.net',
                'vcemsm.atlassian.net',
                'sqlupgrade.atlassian.net',
                'openmethodsdev.atlassian.net',
                'metametrics.atlassian.net',
                'rcmusic.atlassian.net',
                '32796337.atlassian.net',
                'flintfox.atlassian.net',
                'mitigationmeasures.atlassian.net',
                'goodrequest.atlassian.net',
                'easupercharge.atlassian.net',
                'itpldev.atlassian.net',
                'waachosen.atlassian.net',
                'skywavetech.atlassian.net',
                'artgunapp.atlassian.net',
                'jobbook.atlassian.net',
                'spokecommerce.atlassian.net',
                'frontline-consultancy.atlassian.net',
                'socialpylon.atlassian.net',
                'openwebsite.atlassian.net',
                'powerhousedev.atlassian.net',
                'ontargetcloud.atlassian.net',
                'footprints.atlassian.net',
                'imaxpv.atlassian.net',
                'gbanga.atlassian.net',
                'stormreply.atlassian.net',
                'fee-dev.atlassian.net',
                'forensic.atlassian.net',
                'socialprogress.atlassian.net',
                'webrevolve.atlassian.net',
                'globusrobotics.atlassian.net',
                'cloudtestr.atlassian.net',
                'newwave.atlassian.net',
                'bookvideo.atlassian.net',
                'tjadczyk.atlassian.net',
                'dc-systems.atlassian.net',
                'audiblemagic.atlassian.net',
                'jirarag.atlassian.net',
                'krmaximo.atlassian.net',
                'solutionlab.atlassian.net',
                'touchpointpromotions.atlassian.net',
                'entropay.atlassian.net',
                'assurance.atlassian.net',
                'cloudsense.atlassian.net',
                'tablerunner.atlassian.net',
                'betaout.atlassian.net',
                'jktmobility.atlassian.net',
                'clearsoftware.atlassian.net',
                'weplan.atlassian.net',
                'conx2share.atlassian.net',
                'valuefirst.atlassian.net',
                'trippelm.atlassian.net',
                'gammasales.atlassian.net',
                'flixapp.atlassian.net',
                'melvinbaskin.atlassian.net',
                'cegedim-insurance.atlassian.net',
                'elemetrica.atlassian.net',
                'inno360.atlassian.net',
                'datacrm.atlassian.net',
                'latourism.atlassian.net',
                'startvr.atlassian.net',
                'webandcrafts.atlassian.net',
                'capiot.atlassian.net',
                'bqetrack.atlassian.net',
                'google-tieto.atlassian.net',
                'allweb-dev.atlassian.net',
                'arbisoft.atlassian.net',
                'ict-architecture.atlassian.net',
                'gynzykids.atlassian.net',
                'keyruscomputing.atlassian.net',
                'visitcube.atlassian.net',
                'leftrightmind.atlassian.net',
                'nxpiam.atlassian.net',
                'nomadconnection.atlassian.net',
                'unifiedlogic.atlassian.net',
                'aflcioanalytics.atlassian.net',
                'xpectrum.atlassian.net',
                'j9tech.atlassian.net',
                'horizonindustries.atlassian.net',
                'ciscor.atlassian.net',
                'gcmarketing.atlassian.net',
                'brighton.atlassian.net',
                'alterate.atlassian.net',
                'yrcarchitecture.atlassian.net',
                'soporte.atlassian.net',
                'enthrallsports.atlassian.net',
                'glukygroup.atlassian.net',
                'rhaptracker.atlassian.net',
                'phoenixnl.atlassian.net',
                'telefield.atlassian.net',
                'phm-jira01.atlassian.net',
                'wundermandc.atlassian.net',
                'iomproj.atlassian.net',
                'xtreamit.atlassian.net',
                'pathinteractive.atlassian.net',
                'contentcast.atlassian.net',
                'synergistdev.atlassian.net',
                '4am-deco.atlassian.net',
                'flexngate.atlassian.net',
                'printernet.atlassian.net',
                'bdwprojects.atlassian.net',
                'frognerhouse.atlassian.net',
                'sweetspot.atlassian.net',
                'kristeligtdagblad.atlassian.net',
                'visitech.atlassian.net',
                'sparkamaris.atlassian.net',
                'sctrial.atlassian.net',
                'greedygame.atlassian.net',
                'galigeo.atlassian.net',
                'equipesolucoes.atlassian.net',
                'aurrion.atlassian.net',
                'geocgi.atlassian.net',
                'initium.atlassian.net',
                'loyaltynz.atlassian.net',
                'gigato.atlassian.net',
                'smavcs.atlassian.net',
                'content.atlassian.net',
                'scbftw.atlassian.net',
                'getac-rsbu.atlassian.net',
                'mindatlas.atlassian.net',
                'victoriaracingclub.atlassian.net',
                'hboschautomation.atlassian.net',
                'iskraemeco.atlassian.net',
                'raksystems.atlassian.net',
                'wenz-swim-portaal.atlassian.net',
                'cpashleydeloitte.atlassian.net',
                'bswing.atlassian.net',
                'kdevelopers.atlassian.net',
                'svartaekni.atlassian.net',
                'gritdigital.atlassian.net',
                'funcominc.atlassian.net',
                'lippert.atlassian.net',
                'rhlabs.atlassian.net',
                'agility.atlassian.net',
                'leansystem.atlassian.net',
                'alpiqdev.atlassian.net',
                'latticedxb.atlassian.net',
                'crisiscentre.atlassian.net',
                'sodatech.atlassian.net',
                'fredfullsix.atlassian.net',
                'argusfc.atlassian.net',
                'mobilemo.atlassian.net',
                'brockhaus.atlassian.net',
                'creativejar.atlassian.net',
                'flexmirantis.atlassian.net',
                'busways.atlassian.net',
                'imfjira.atlassian.net',
                'indusaction.atlassian.net',
                'udfutvikling.atlassian.net',
                'itcompaniet.atlassian.net',
                'systemautomation.atlassian.net',
                'toddevelopment.atlassian.net',
                'gaurangbhatt.atlassian.net',
                'kathytolearn.atlassian.net',
                'gsmarketing.atlassian.net',
                'evrysweden.atlassian.net',
                'ibmbassa.atlassian.net',
                'dccaffsdcs.atlassian.net',
                'ivhstat.atlassian.net',
                'zesiummobile.atlassian.net'
                )
                
                ; 
                
                
  