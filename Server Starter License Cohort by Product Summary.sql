with sale_hist as
(
select email_domain, min(date) as first_prod
from sale
group by 1
order by 1
),
base_pop as
(
select a.month, a.email_domain, a.email, a.platform, a.country, a.region, a.date, a.sen,
 case when b.email_domain is not null then 'n2n-starter' else 'n2e-starter' end as starter_type
from sale as a
left join sale_hist as b on (a.email_domain=b.email_domain and a.date = b.first_prod)
where base_product in ('JIRA','JIRA Software')
and month between '2015-03' and '2016-05'
and sale_type = 'Starter'
and sale_action = 'New'
and platform = 'Server'
)
, active_starters as
(
select flp.sen
from fact_license_period flp
inner join dim_license l on l.license_id = flp.current_license_id
inner join dim_date d1 on d1.date_id = flp.current_period_created_date_id
inner join dim_date d2 on d2.date_id = flp.current_period_end_date_id
inner join dim_product p on p.product_id = flp.product_id
where l.level = 'Starter'
and p.base_product like 'JIRA%'
and now() >= d1.date_value
and now() < d2.date_value
)
,
final_pop as 
(
select distinct a.sen, a.email, a.region, a.country,
case
 when company_size is null then 'unknown'
 when company_size<=10 then '1-10'
 when company_size between 11 and 50 then '11-50'
 when company_size between 51 and 500 then '51-500'
 when company_size > 500 then '500+'
end as company_size, starter_type,
case when a.date < '2015-10-06' then 'Pre-renaissance' else 'Post-renaissance' end as period
from base_pop a
join playground_bizops.customer_size as c on a.email_domain = c.email_domain
join active_starters as d on a.sen = d.sen
)
select count(sen)
from final_pop

   -- case when b.sale_type = 'New to Existing' then 1 else 0 end as converted