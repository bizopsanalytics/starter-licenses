with experiment_group as 
(
select distinct a.event,
                a.sen, 
                a.instance, 
                b.tech_email_domain as email_domain
from raw_product.ondemand_event_summary as a
left join public.license as b on a.instance = b.hosted_url
where a.product='jira' 
      and a.date>='2016-05-17'
      and a.event in ('grow2894.ENGAGE-394.experiment.triggered.a','grow2894.ENGAGE-394.experiment.triggered.control')
order by 1
)
select a.*, b.company_size
from experiment_group as a
left join zone_bizops.customer_size as b on a.email_domain = b.email_domain

