-- company size data comes from a one-off pull, Kelson knows how to update it.

with s as (
select distinct platform, month, email_domain
from sale
where base_product in ('JIRA','JIRA Core','JIRA Software')
and month > '2015-02'
and month not in ('2016-04','2016-05') --This can be changed when the data is updated
and sale_type = 'Starter'
and sale_action = 'New'
group by platform, month, email_domain)
select month, 
count(distinct case when company_size >5000 then s.email_domain end) as t_10000,
count(distinct case when company_size between 501 and 5000 then s.email_domain end) as t_5000,
count(distinct case when company_size between 51 and 500 then s.email_domain end) as t_500,
count(distinct case when company_size = 50 then s.email_domain end) as t_50,
count(distinct case when company_size = 10 then s.email_domain end) as t_10,
count(distinct case when company_size is null then s.email_domain end) as t_unknown
from s left join playground_bizops.customer_size c on (s.email_domain = c.email_domain)
where s.platform = 'Server'
group by 1
order by 1;