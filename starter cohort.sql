with sale_hist as
(
select email_domain, min(date) as first_prod
from sale
group by 1
order by 1
),
active_users as
(
select sen, l.parent_sen, date_month(collection_date) as month, jira_users as enabled_users
from unicorn_instance_stats u inner join license l on (u.instance = l.hosted_url)
where collection_date in ('2016-05-01')  --replace '2016-03-21' with date_trunc('week',current_date) when final population
  and '2016-05-01' between start_date and expiry_date  --replace '2016-03-21' with date_trunc('week',current_date) when final population
),
base_pop as
(
select a.month, a.email_domain, a.email, a.platform, a.country, a.region, a.date, a.sen,
 case when b.email_domain is not null then 'n2n-starter' else 'n2e-starter' end as starter_type
from sale as a
left join sale_hist as b on (a.email_domain=b.email_domain and a.date = b.first_prod)
where base_product in ('JIRA','JIRA Software')
and month between '2015-02' and '2016-03'
and sale_type = 'Starter'
and sale_action = 'New'
and platform = 'Cloud'
)
, active_starters as
(
select flp.sen
from fact_license_period flp
inner join dim_license l on l.license_id = flp.current_license_id
inner join dim_date d1 on d1.date_id = flp.current_period_created_date_id
inner join dim_date d2 on d2.date_id = flp.current_period_end_date_id
inner join dim_product p on p.product_id = flp.product_id
where l.level = 'Starter'
and p.base_product like 'JIRA%'
and now() >= d1.date_value
and now() < d2.date_value
)
select distinct a.sen, b.parent_sen, a.email, a.email_domain, a.region, a.country,
case
 when company_size is null then 'unknown'
 when company_size<=10 then '1-10'
 when company_size between 11 and 50 then '11-50'
 when company_size between 51 and 500 then '51-500'
 when company_size > 500 then '500+'
end as company_size, b.enabled_users, starter_type,
case when a.date < '2015-10-06' then 'Pre-renaissance' else 'Post-renaissance' end as period
from base_pop a
join active_users as b on cast(a.sen as bigint) = b.sen
join playground_bizops.customer_size as c on a.email_domain = c.email_domain
join active_starters as d on a.sen = d.sen
where b.enabled_users between 5 and 10
and c.company_size >=11